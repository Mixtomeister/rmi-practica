/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.mixtomeister.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Mixtomeister
 */
public class RMIClient extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/es/mixtomeister/client/Interface.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Remote Calc");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        
        /*try {
            Registry reg = LocateRegistry.getRegistry("127.0.0.1", 4455);
            InterfaceRMICalc calc = (InterfaceRMICalc) reg.lookup("rmi://localhost//RMICalc");
            System.out.println("Suma: " + calc.sumar(5, 5));
        } catch (NotBoundException | RemoteException ex) {
            System.out.println("No se ha podido acceder al objecto remoto.");
        }*/
    }
    
}
