package es.mixtomeister.client;

import es.mixtomeister.server.InterfaceRMICalc;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Mixtomeister
 */
public class InterfaceController implements Initializable {
    
    @FXML
    private Button btn_sum;
    
    @FXML
    private TextField sum1;
    @FXML
    private TextField sum2;
    
    @FXML
    private Label res;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btn_sum.setOnAction(e -> remoteSum());
    }
    
    private void remoteSum(){
        try {
            Registry reg = LocateRegistry.getRegistry("127.0.0.1", 4455);
            InterfaceRMICalc calc = (InterfaceRMICalc) reg.lookup("rmi://localhost//RMICalc");
            res.setText(String.valueOf(calc.sumar(Integer.parseInt(sum1.getText()), Integer.parseInt(sum2.getText()))));
        } catch (NotBoundException | RemoteException ex) {
            error("Error", "No se ha podido acceder al objecto remoto.");
        } catch (NumberFormatException ex){
            error("Error", "Introducidos número erroneos");
        }
    }
    
    public void error(String title, String body) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(body);
        alert.showAndWait();
    }
    
}
