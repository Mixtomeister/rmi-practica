package es.mixtomeister.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Mixtomeister
 */
public interface InterfaceRMICalc extends Remote {
    int sumar(int sum1, int sum2) throws RemoteException;
}
