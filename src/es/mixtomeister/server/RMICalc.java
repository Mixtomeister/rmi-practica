/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.mixtomeister.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Mixtomeister
 */
public class RMICalc extends UnicastRemoteObject implements InterfaceRMICalc{

    public RMICalc() throws RemoteException {
        super();
    }
    
    @Override
    public int sumar(int sum1, int sum2) throws RemoteException {
        return sum1 + sum2;
    }
    
    
    public static void main(String[] args) {
        try {
            Registry reg = LocateRegistry.createRegistry(4455);
            reg.rebind("rmi://localhost//RMICalc", new RMICalc());
            System.out.println("Servidor iniciado");
        } catch (RemoteException ex) {
            System.out.println("Error a iniciar el servidor.");
        }
    }
}
